let D2L = require("valence");
const Axios = require("axios");
const handleError = require("../helpers/errorHandler").handleError;

exports.getFullApiUrl = function (httpMethod, apiPath) {
	let appContext = new D2L.ApplicationContext(
		process.env.LMS_APP_ID,
		process.env.LMS_APP_KEY
	);
	let userContext = appContext.createUserContextWithValues(
		process.env.LMS_HOST,
		process.env.LMS_PORT,
		process.env.LMS_USER_ID,
		process.env.LMS_USER_KEY
	);

	return userContext.createAuthenticatedUrl(apiPath, httpMethod);
};

exports.findFirstItemInPagedResult = function (
	url,
	propertyToLookAt,
	itemToFind,
	propertyToReturn,
	orgTypeId = null
) {
	return new Promise(async function (resolve, reject) {
		let bookmark = null;
		let hasMore = true;
		while (hasMore) {
			try {
				let data = await exports.getPagedResult(url, bookmark, orgTypeId);

				bookmark = data.PagingInfo.Bookmark;
				hasMore = data.PagingInfo.HasMoreItems;

				const filtered = data.Items.filter(
					(item) => item[propertyToLookAt] === itemToFind
				);
				if (filtered.length > 0) {
					return resolve(filtered[0][propertyToReturn]);
				} else if (!hasMore) {
					return resolve(null);
				}
			} catch (e) {
				return reject(e);
			}
		}
	});
};

exports.getAllItemsFromPagedObjectList = function (url, itemLimit = 0) {
	return new Promise(async function (resolve, reject) {
		let data = [];
		let nextUrl = url;
		while (nextUrl !== null && (itemLimit === 0 || data.length < itemLimit)) {
			try {
				let pageData = await exports.getPagedObjectList(nextUrl);

				nextUrl = pageData.Next;
				data = data.concat(pageData.Objects);
			} catch (e) {
				return reject(e);
			}
		}

		return resolve(data);
	});
};

exports.getPagedObjectList = function (url) {
	const authedUrl = exports.getFullApiUrl("GET", url);

	return new Promise(function (resolve, reject) {
		Axios.get(authedUrl)
			.then(function (res) {
				resolve(res.data);
			})
			.catch(function (err) {
				reject(handleError(err));
			});
	});
};

exports.getPagedResult = function (url, bookmark = null, orgTypeId = null) {
	let params = {};
	let paramsUrl = "";

	if (bookmark) {
		params.bookmark = bookmark;
	}
	if (orgTypeId) {
		params.ouTypeId = orgTypeId;
	}

	for (let param in params) {
		if (params.hasOwnProperty(param)) {
			paramsUrl += "&" + param + "=" + params[param];
		}
	}
	if (paramsUrl.length > 0) {
		paramsUrl = "?" + paramsUrl.substr(1);
	}

	const authedUrl = exports.getFullApiUrl("GET", url + paramsUrl);

	return new Promise(function (resolve, reject) {
		Axios.get(authedUrl)
			.then(function (res) {
				resolve(res.data);
			})
			.catch(function (err) {
				reject(handleError(err));
			});
	});
};

exports.getPagedResultWithQueryParams = function (
	url,
	urlQueryParams = {},
	bookmark = null
) {
	let paramsUrl = "";

	if (bookmark) {
		urlQueryParams.bookmark = bookmark;
	}

	for (let param in urlQueryParams) {
		if (urlQueryParams.hasOwnProperty(param)) {
			paramsUrl += "&" + param + "=" + urlQueryParams[param];
		}
	}
	if (paramsUrl.length > 0) {
		paramsUrl = "?" + paramsUrl.substr(1);
	}

	const authedUrl = exports.getFullApiUrl("GET", url + paramsUrl);

	return new Promise(function (resolve, reject) {
		Axios.get(authedUrl)
			.then(function (res) {
				resolve(res.data);
			})
			.catch(function (err) {
				reject(handleError(err));
			});
	});
};

exports.getAllItemsFromPagedResultSet = function (
	url,
	urlQueryParams = [],
	itemLimit = 0
) {
	return new Promise(async function (resolve, reject) {
		let bookmark = null;
		let hasMore = true;
		let items = [];

		while (hasMore && (itemLimit === 0 || items.length < itemLimit)) {
			try {
				let data = await exports.getPagedResultWithQueryParams(
					url,
					urlQueryParams,
					bookmark
				);

				bookmark = data.PagingInfo.Bookmark;
				hasMore = data.PagingInfo.HasMoreItems;

				data.Items.forEach(function (item) {
					items.push(item);
				});

				if (!hasMore) {
					return resolve(items);
				}
			} catch (e) {
				return reject(e);
			}
		}
	});
};
