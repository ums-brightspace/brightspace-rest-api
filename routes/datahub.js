const express = require("express");
const router = express.Router();
const datahubController = require("../controllers/datahub");
const logger = require("../services/logger");
const dateUtil = require("../services/date");

router.get("/:plugin", function (req, res) {
	let plugin = req.params.plugin;

	datahubController
		.getPluginDetails(plugin)
		.then(function (data) {
			res.status(200).json(data);
		})
		.catch(function (err) {
			logger.info("Data Hub error: ", {
				plugin: plugin,
				errors: err,
			});
			res.status(400).json({
				plugin: plugin,
				errors: err,
			});
		});
});

router.get("/:plugin/available", function (req, res) {
	let plugin = req.params.plugin;

	datahubController
		.getDownloadsAvailableByPlugin(plugin)
		.then(function (data) {
			res.status(200).json(data);
		})
		.catch(function (err) {
			logger.info("Data Hub error: ", {
				plugin: plugin,
				errors: err,
			});
			res.status(400).json({
				plugin: plugin,
				errors: err,
			});
		});
});

router.get("/:plugin/:key", function (req, res) {
	let plugin = req.params.plugin;
	let key = req.params.key;
	datahubController
		.getPluginFile(plugin, key)
		.then(function (data) {
			const strm = data.data;
			res.setHeader("content-type", "application/zip; charset=utf-8");
			res.setHeader(
				"Content-Disposition",
				'attachment; filename="' +
					plugin +
					"_" +
					dateUtil.fullDateString() +
					'.zip"'
			);
			strm.on("data", (chunk) => {
				res.write(new Buffer.from(chunk));
			});
			strm.on("end", () => {
				res.status(200).end();
			});
		})
		.catch(function (err) {
			logger.info("Data Hub error: ", {
				plugin: plugin,
				errors: err,
			});
			res.status(400).json({
				plugin: plugin,
				errors: err,
			});
		});
});

module.exports = router;
