module.exports = getData(request.body);

function getData(reqBody) {
	const data = JSON.parse(reqBody);
	return {
		Identifier: 6939,
		Name: data.Name,
		Code: data.Code,
		IsActive: true,
		Path: "/content/enforced/6939-2020.UMS01-C.12345.1/",
		StartDate: null,
		EndDate: null,
		CourseTemplate: {
			Identifier: data.CourseTemplateId,
			Name: "User Sandboxes Container",
			Code: "ct_user_sandboxes_cb",
		},
		Semester: null,
		Department: {
			Identifier: 6650,
			Name: "Brightspace Learning Area*",
			Code: "dept_Brightspace_Training_d2l",
		},
	};
}
