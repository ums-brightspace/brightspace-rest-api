const Axios = require("axios");
const logger = require("../services/logger");
const rateLimiter = require("axios-rate-limit");
const handleError = require("./errorHandler").handleError;

const throttleThreshold = 0.25;
const unThrottleThreshold = 0.5;
const throttlePercentage = 0.5;
const unThrottlePercentage = 1.25;
const criticalThreshold = 0.1;
let maxRequests = null;
let rlAxios = null;
let rlRemain = Number(process.env.LMS_RATE_LIMIT_MAX);
let rlReset = Number(process.env.LMS_RATE_LIMIT_TIME) / 1000;
let rlAppState = "ready";

function initAxios() {
	maxRequests = process.env.LMS_RATE_LIMIT_MAX;

	let axios = {};
	if (process.env.PROXY_HOST) {
		axios = Axios.create({
			timeout: Number(process.env.HTTP_REQUEST_TIMEOUT),
			proxy: {
				host: process.env.PROXY_HOST,
				port: process.env.PROXY_PORT,
			},
		});
	} else {
		axios = Axios.create({
			timeout: Number(process.env.HTTP_REQUEST_TIMEOUT),
		});
	}

	if (
		process.env.NODE_ENV === "development" ||
		process.env.NODE_ENV === "testing"
	) {
		axios.interceptors.response.use(
			interceptResponseSuccess,
			logErrorsAndIntercept
		);
	} else {
		axios.interceptors.response.use(
			interceptResponseSuccess,
			interceptResponseError
		);
	}

	rlAxios = rateLimiter(axios, {
		maxRequests: maxRequests,
		perMilliseconds: process.env.LMS_RATE_LIMIT_TIME,
	});
}

function logErrorsAndIntercept(err) {
	logger.info("Axios error: ", {
		logType: "axios",
		action: "error",
		method: err.request.method,
		path: err.request.path,
		error: handleError(err),
	});

	return interceptResponseError(err);
}

exports.default = function () {
	if (!rlAxios) {
		initAxios();
	}

	return rlAxios;
};

exports.rateLimitValues = {
	rlRemain: function () {
		return rlRemain;
	},
	rlReset: function () {
		return rlReset;
	},
	appState: function () {
		return rlAppState;
	},
};

function interceptResponseSuccess(res) {
	updateRateLimit(res);

	return res;
}

function interceptResponseError(err) {
	if (err.response) {
		updateRateLimit(err.response);

		return Promise.reject(err);
	} else {
		updateRateLimit(null);
		return Promise.reject(err);
	}
}

function updateRateLimit(response) {
	const maxRL = process.env.LMS_RATE_LIMIT_MAX;
	let rl = maxRL;
	if (response) {
		rl = getRateLimitRemaining(response);
	}
	rlRemain = rl;
	rlReset = response.headers[process.env.LMS_RATE_LIMIT_RESET_HEADER];
	if (response.status === 429) {
		rlAppState = "halt";
		rlReset = response.headers[process.env.LMS_RATE_LIMIT_RETRY_AFTER_HEADER];
	}

	if (rl <= maxRL * criticalThreshold) {
		rlAppState = "critical";
		decreaseMaxRequestsAllowed(1);
	} else if (rl <= maxRL * throttleThreshold) {
		rlAppState = "throttle";
		decreaseMaxRequestsAllowed(rl);
	} else if (maxRequests !== maxRL && rl >= maxRL * unThrottleThreshold) {
		rlAppState = "ready";
		increaseMaxRequestsAllowed(rl);
	}
}

function increaseMaxRequestsAllowed(rateLimitRemaining) {
	const maxRL = process.env.LMS_RATE_LIMIT_MAX;
	let newMaxRequests = Math.ceil(maxRequests * unThrottlePercentage);
	if (newMaxRequests > maxRL) {
		newMaxRequests = maxRL;
	}

	logger.info("Un-Throttling connections: ", {
		logType: "axios",
		action: "unthrottle",
		currentMaxRequests: maxRequests,
		newMaxRequests: newMaxRequests,
		rateLimitRemaining: rateLimitRemaining,
	});

	maxRequests = newMaxRequests;
	setNewRateLimit();
}

function decreaseMaxRequestsAllowed(rateLimitRemaining) {
	let newMaxRequests = Math.ceil(maxRequests * throttlePercentage);
	if (newMaxRequests < 1) {
		newMaxRequests = 1;
	}

	logger.warn("Throttling connections: ", {
		logType: "axios",
		action: "throttle",
		currentMaxRequests: maxRequests,
		newMaxRequests: newMaxRequests,
		rateLimitRemaining: rateLimitRemaining,
	});

	maxRequests = newMaxRequests;
	setNewRateLimit();
}

function setNewRateLimit() {
	rlAxios.setRateLimitOptions({
		maxRequests: maxRequests,
		perMilliseconds: process.env.LMS_RATE_LIMIT_TIME,
	});
}

function getRateLimitRemaining(response) {
	if (response.status === 429) {
		return 1;
	}

	const rateLimitRemain = process.env.LMS_RATE_LIMIT_REMAIN_HEADER;
	if (response && response.headers) {
		return Math.floor(response.headers[rateLimitRemain] / 10);
	}

	return process.env.LMS_RATE_LIMIT_MAX;
}
