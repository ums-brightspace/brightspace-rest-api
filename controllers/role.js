const enrollment = require("../controllers/enrollment");
const LMS = require("../services/lms");
const helper = require("../helpers/errorHandler");
const Axios = require("../helpers/axios").default();

exports.setRole = function (role) {
	return new Promise(function (resolve, reject) {
		if (!role) {
			reject({
				error: "Role not provided",
			});
		}

		let errs = getRoleErrors(role);
		if (errs.length > 0) {
			reject(errs);
		}

		let roleId = enrollment.getRoleIdByCode(role.role);
		if (Number.isInteger(parseInt(role.role, 10))) {
			roleId = role.role;
		}
		const umsOrgId = process.env.LMS_UMS_ORG_ID;

		enrollment
			.getUserRole(umsOrgId, role.userLmsId)
			.then(function (lmsRole) {
				if (lmsRole?.IsCascading) {
					return enrollment.cascadeDeleteEnrollments(role.userLmsId, umsOrgId);
				}
			})
			.then(function () {
				return enrollment.addSystemEnrollment(umsOrgId, role.userLmsId, roleId);
			})
			.then(function (enrollmentData) {
				if (enrollmentData.isCascading) {
					return;
				}
				return enrollment.enrollUserInCampusOrgs(role.userLmsId);
			})
			.then(function () {
				resolve();
			})
			.catch(function (err) {
				reject(err);
			});
	});
};

exports.getAllRoles = function () {
	const url = "/d2l/api/lp/1.31/roles/";

	return new Promise(function (resolve, reject) {
		let authedUrl = LMS.getFullApiUrl("GET", url);
		Axios.get(authedUrl)
			.then(function (res) {
				return resolve(res.data);
			})
			.catch(function (err) {
				reject(helper.handleError(err));
			});
	});
};

function getRoleErrors(role) {
	let errs = [];

	if (!role.hasOwnProperty("userLmsId")) {
		errs.push({
			error: "user lms id missing",
		});
	}

	if (!role.hasOwnProperty("role")) {
		errs.push({
			error: "role missing",
		});
	} else {
		const intRole = parseInt(role.role, 10);
		if (!isNaN(intRole)) {
			if (intRole < 100 || intRole > 1000) {
				errs.push({
					error: "Unknown role id",
				});
			}
		} else if (typeof role.role === "string" || role.role instanceof String) {
			if (
				!(
					role.role === "S" ||
					role.role === "I" ||
					role.role === "N" ||
					role.role === "X" ||
					role.role === "T" ||
					role.role === "P" ||
					role.role === "Q"
				)
			) {
				errs.push({
					error:
						"type can only be S (Student) or I (Instructor) or N(Instructor - New Hire) or X (Incomplete Student) or T (Teaching Assistant)",
				});
			}
		} else {
			errs.push({
				error: "Unknown role",
			});
		}
	}

	return errs;
}
