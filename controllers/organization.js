const Axios = require("../helpers/axios").default();
const LMS = require("../services/lms");
const helper = require("../helpers/errorHandler");

exports.getId = function (parentId, orgType, orgCode) {
	return new Promise(function (resolve, reject) {
		getId(parentId, orgType, orgCode)
			.then(function ReturnId(id) {
				resolve(id);
			})
			.catch(function ReportAnyErrors(errs) {
				reject(errs);
			});
	});
};

exports.isOrgPresent = function (parentId, orgType, orgCode) {
	return new Promise(function (resolve, reject) {
		getId(parentId, orgType, orgCode)
			.then(function (id) {
				resolve(id);
			})
			.catch(function (err) {
				reject(err);
			});
	});
};

function getId(parent, type, code) {
	let orgTypeId = getOrgTypeId(type);
	return new Promise(function (resolve, reject) {
		let url = "/d2l/api/lp/1.25/orgstructure/" + parent + "/children/paged/";

		LMS.findFirstItemInPagedResult(url, "Code", code, "Identifier", orgTypeId)
			.then(function (id) {
				resolve(id);
			})
			.catch(function (err) {
				reject(err);
			});
	});
}

exports.add = function (parentId, orgType, org) {
	return new Promise(function (resolve, reject) {
		if (!org) {
			return reject({
				error: "Org not provided",
			});
		}

		getErrors(org)
			.then(function AddToLMS(org) {
				return addToLMS(parentId, getOrgTypeId(orgType), org);
			})
			.then(function ReturnLmsId(lmsId) {
				resolve(lmsId);
			})
			.catch(function ReportAnyErrors(errs) {
				reject(errs);
			});
	});
};

function addToLMS(parentId, orgTypeId, org) {
	return new Promise(function (resolve, reject) {
		let authedUrl = LMS.getFullApiUrl("POST", "/d2l/api/lp/1.25/orgstructure/");
		let data = {
			Type: parseInt(orgTypeId),
			Name: org.name,
			Code: org.code,
			Parents: [parseInt(parentId)],
		};

		Axios.post(authedUrl, data)
			.then(function (res) {
				resolve(res.data.Identifier);
			})
			.catch(function (err) {
				reject(helper.handleError(err));
			});
	});
}

function getErrors(org) {
	let errs = [];
	if (!org.hasOwnProperty("name")) {
		errs.push({
			error: "name missing",
		});
	}
	if (!org.hasOwnProperty("code")) {
		errs.push({
			error: "code missing",
		});
	}

	return new Promise(function (resolve, reject) {
		if (errs.length > 0) {
			reject(errs);
		} else {
			resolve(org);
		}
	});
}

function getOrgTypeId(orgType) {
	return process.env["LMS_" + orgType.toUpperCase() + "_TYPE_ID"];
}
